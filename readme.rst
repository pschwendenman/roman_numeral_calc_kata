--------------------------
Roman Numeral Calculator
--------------------------

Tests
======

To run the tests::

    make test

Methodology
============

Method for Addition
--------------------

#. Validate both numerals
#. Convert both numerals to integers
#. Sum the integers
#. Validate sum in within range.
#. Convert the integers back to Roman numerals

Method for Subtraction
-----------------------

Subtraction is the exact same as addition except the integers are subtracted
rather than added.

Requirements
=============

In order to build the project, you should be sure to install ``pkg-config`` and
the ``check`` library.

Library API
============

The main public API for the stories is in `roman.h`

roman.h
---------

Addition
#########

.. code-block:: C

    void add_roman(char* first, char* second, char* answer);

The inputs to this function should be three ``char`` arrays. Ensure that the
last argument is at least 16 characters long to accommodate the longest valid
roman numeral "MMMDCCCLXXXVIII" which is 15 characters.

Subtraction
#############

.. code-block:: C

    void subtract_roman(char* first, char* second, char* answer);

The inputs to this function should be three ``char`` arrays. Ensure that the
last argument is at least 16 characters long to accommodate the longest valid
roman numeral "MMMDCCCLXXXVIII" which is 15 characters.

conversion.h
-------------

Convert to Roman Numeral
#########################

.. code-block:: C

    void convert_to_roman(unsigned short int arabic_number, char *roman_numeral);

Ensure that the last argument is at least 16 characters long to accommodate
the longest valid roman numeral "MMMDCCCLXXXVIII" which is 15 characters.

Convert from Roman Numeral
###########################

.. code-block:: C

    int convert_from_roman(char *roman_numeral);

Macros
#######

conversion.h also contains to helper macros ``append_digit`` and
``append_arabic`` which are not available to users of the library. They serve
to reduce duplicated logic and help show the pattern of parsing and creating
roman numerals.

validator.h
------------

Validation
###########

.. code-block:: C

    int validate_roman(char* roman_numeral);

This function returns zero for invalid numerals and a non-zero value for valid
numerals.

Regular Expression for Matching Numerals
#########################################

.. code-block:: C

    static const char roman_pattern[] = "^M{0,3}(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[VX]|V?I{0,3})$";

The regular expression breaks into four parts

#. Matching numerals for 1000, 2000, and 3000: ``M{0,3}``
#. Matching numerals for 100, 200, ... 900: ``C[MD]|D?C{0,3}``
#. Matching numerals for 10, 20, ... 90: ``X[CL]|L?X{0,3}``
#. Matching numerals for 1, 2, 3, ... 9: ``I[VX]|V?I{0,3}``
