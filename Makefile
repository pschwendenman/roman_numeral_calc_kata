CC = gcc
CFLAGS = --pedantic -Wall -std=c99 -O2
LIBS = `pkg-config --libs check`

TEST = tests/check_roman.c
OBJS = roman.o check_roman.o conversion.o validator.o

%.o: src/%.c src/%.h
	gcc -c $(CFLAGS) -o $@ $<

check_roman.o: tests/check_roman.c $(wildcard tests/*tests.c)
	gcc -c $(CFLAGS) -o $@ $<

.PHONY: test check clean

test: check

check: check_roman
	@./check_roman

check_roman: ${OBJS}
	$(CC) $(CFLAGS) -o $@ ${OBJS} $(LIBS)

clean:
	rm -f $(OBJS) check_roman
