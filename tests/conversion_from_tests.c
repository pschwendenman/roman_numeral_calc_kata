#include <check.h>
#include "../src/conversion.h"

START_TEST(test_roman_convert_one)
{
    char roman[16];
    convert_to_roman(1, roman);
    ck_assert_str_eq(roman, "I");
}
END_TEST

START_TEST(test_roman_convert_two)
{
    char roman[16];
    convert_to_roman(2, roman);
    ck_assert_str_eq(roman, "II");
}
END_TEST

START_TEST(test_roman_convert_four)
{
    char roman[16];
    convert_to_roman(4, roman);
    ck_assert_str_eq(roman, "IV");
}
END_TEST

START_TEST(test_roman_convert_five)
{
    char roman[16];
    convert_to_roman(5, roman);
    ck_assert_str_eq(roman, "V");
}
END_TEST

START_TEST(test_roman_convert_nineteen)
{
    char roman[16];
    convert_to_roman(19, roman);
    ck_assert_str_eq(roman, "XIX");
}
END_TEST

START_TEST(test_roman_convert_thirty_four)
{
    char roman[16];
    convert_to_roman(34, roman);
    ck_assert_str_eq(roman, "XXXIV");
}
END_TEST

START_TEST(test_roman_convert_forty_three)
{
    char roman[16];
    convert_to_roman(43, roman);
    ck_assert_str_eq(roman, "XLIII");
}
END_TEST

START_TEST(test_roman_convert_fifty)
{
    char roman[16];
    convert_to_roman(50, roman);
    ck_assert_str_eq(roman, "L");
}
END_TEST

START_TEST(test_roman_convert_ninety)
{
    char roman[16];
    convert_to_roman(90, roman);
    ck_assert_str_eq(roman, "XC");
}
END_TEST

START_TEST(test_roman_convert_two_hundred)
{
    char roman[16];
    convert_to_roman(200, roman);
    ck_assert_str_eq(roman, "CC");
}
END_TEST

START_TEST(test_roman_convert_four_hundred)
{
    char roman[16];
    convert_to_roman(400, roman);
    ck_assert_str_eq(roman, "CD");
}
END_TEST

START_TEST(test_roman_convert_five_hundred)
{
    char roman[16];
    convert_to_roman(500, roman);
    ck_assert_str_eq(roman, "D");
}
END_TEST

START_TEST(test_roman_convert_nine_hundred)
{
    char roman[16];
    convert_to_roman(900, roman);
    ck_assert_str_eq(roman, "CM");
}
END_TEST

START_TEST(test_roman_convert_three_thousand)
{
    char roman[16];
    convert_to_roman(3000, roman);
    ck_assert_str_eq(roman, "MMM");
}
END_TEST

Suite * make_conversion_from_suite(void) {
    Suite *s;
    TCase *tc_convert_from;

    s = suite_create("convert_from");

    /* Core test case */
    tc_convert_from = tcase_create("from");

    tcase_add_test(tc_convert_from, test_roman_convert_one);
    tcase_add_test(tc_convert_from, test_roman_convert_two);
    tcase_add_test(tc_convert_from, test_roman_convert_four);
    tcase_add_test(tc_convert_from, test_roman_convert_five);
    tcase_add_test(tc_convert_from, test_roman_convert_nineteen);
    tcase_add_test(tc_convert_from, test_roman_convert_thirty_four);
    tcase_add_test(tc_convert_from, test_roman_convert_forty_three);
    tcase_add_test(tc_convert_from, test_roman_convert_fifty);
    tcase_add_test(tc_convert_from, test_roman_convert_ninety);
    tcase_add_test(tc_convert_from, test_roman_convert_two_hundred);
    tcase_add_test(tc_convert_from, test_roman_convert_four_hundred);
    tcase_add_test(tc_convert_from, test_roman_convert_five_hundred);
    tcase_add_test(tc_convert_from, test_roman_convert_nine_hundred);
    tcase_add_test(tc_convert_from, test_roman_convert_three_thousand);

    suite_add_tcase(s, tc_convert_from);

    return s;
}
