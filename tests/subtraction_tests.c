#include <check.h>
#include "../src/roman.h"

START_TEST(difference_of_IV_and_I_is_III)
{
    char roman_numeral[32];

    subtract_roman("IV", "I", roman_numeral);
    ck_assert_str_eq(roman_numeral, "III");
}
END_TEST

START_TEST(difference_of_MCCXIV_and_DCCCXLV_is_CCCLXIX)
{
    char roman_numeral[32];

    subtract_roman("MCCXIV", "DCCCXLV", roman_numeral);
    ck_assert_str_eq(roman_numeral, "CCCLXIX");
}
END_TEST

Suite * make_subtraction_suite(void) {
    Suite *s;
    TCase *tc_subtract;

    s = suite_create("roman_sub");

    /* Core test case */
    tc_subtract = tcase_create("subtraction");

    tcase_add_test(tc_subtract, difference_of_IV_and_I_is_III);
    tcase_add_test(tc_subtract, difference_of_MCCXIV_and_DCCCXLV_is_CCCLXIX);

    suite_add_tcase(s, tc_subtract);

    return s;
}
