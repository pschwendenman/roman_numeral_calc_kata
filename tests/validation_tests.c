#include <check.h>
#include "../src/validator.h"

START_TEST(test_validate_I) {
    int validation_result = validate_roman("I");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_invalidate_G) {
    int validation_result = validate_roman("G");

    ck_assert(!validation_result);
} END_TEST

START_TEST(test_validate_III) {
    int validation_result = validate_roman("III");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_invalidate_IIII) {
    int validation_result = validate_roman("IIII");

    ck_assert(!validation_result);
} END_TEST

START_TEST(test_validate_IV) {
    int validation_result = validate_roman("IV");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_V) {
    int validation_result = validate_roman("V");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_VIII) {
    int validation_result = validate_roman("VIII");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_IX) {
    int validation_result = validate_roman("IX");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_LXX) {
    int validation_result = validate_roman("LXX");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_XCII) {
    int validation_result = validate_roman("XCII");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_XLIV) {
    int validation_result = validate_roman("XLIV");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_CDIV) {
    int validation_result = validate_roman("CDIV");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_CMII) {
    int validation_result = validate_roman("CMII");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_DCCXX) {
    int validation_result = validate_roman("DCCXX");

    ck_assert(validation_result);
} END_TEST

START_TEST(test_validate_MM) {
    int validation_result = validate_roman("MM");

    ck_assert(validation_result);
} END_TEST

Suite * make_validation_suite(void) {
    Suite *s;
    TCase *tc_validate;

    s = suite_create("validate");

    /* Core test case */
    tc_validate = tcase_create("validation");

    tcase_add_test(tc_validate, test_validate_I);
    tcase_add_test(tc_validate, test_validate_III);
    tcase_add_test(tc_validate, test_invalidate_G);
    tcase_add_test(tc_validate, test_invalidate_IIII);
    tcase_add_test(tc_validate, test_validate_IV);
    tcase_add_test(tc_validate, test_validate_V);
    tcase_add_test(tc_validate, test_validate_VIII);
    tcase_add_test(tc_validate, test_validate_IX);
    tcase_add_test(tc_validate, test_validate_XLIV);
    tcase_add_test(tc_validate, test_validate_XCII);
    tcase_add_test(tc_validate, test_validate_LXX);
    tcase_add_test(tc_validate, test_validate_CDIV);
    tcase_add_test(tc_validate, test_validate_CMII);
    tcase_add_test(tc_validate, test_validate_DCCXX);
    tcase_add_test(tc_validate, test_validate_MM);

    suite_add_tcase(s, tc_validate);

    return s;
}
