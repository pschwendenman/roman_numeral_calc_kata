#include <check.h>
#include "../src/conversion.h"

START_TEST(test_convert_I_to_arabic)
{
    int number;
    number = convert_from_roman("I");
    ck_assert_int_eq(number, 1);
}
END_TEST

START_TEST(test_convert_II_to_arabic)
{
    int number;
    number = convert_from_roman("II");
    ck_assert_int_eq(number, 2);
}
END_TEST

START_TEST(test_convert_IV_to_arabic)
{
    int number;
    number = convert_from_roman("IV");
    ck_assert_int_eq(number, 4);
}
END_TEST

START_TEST(test_convert_V_to_arabic)
{
    int number;
    number = convert_from_roman("V");
    ck_assert_int_eq(number, 5);
}
END_TEST

START_TEST(test_convert_IX_to_arabic)
{
    int number;
    number = convert_from_roman("IX");
    ck_assert_int_eq(number, 9);
}
END_TEST

START_TEST(test_convert_X_to_arabic)
{
    int number;
    number = convert_from_roman("X");
    ck_assert_int_eq(number, 10);
}
END_TEST

START_TEST(test_convert_XL_to_arabic)
{
    int number;
    number = convert_from_roman("XL");
    ck_assert_int_eq(number, 40);
}
END_TEST

START_TEST(test_convert_L_to_arabic)
{
    int number;
    number = convert_from_roman("L");
    ck_assert_int_eq(number, 50);
}
END_TEST

START_TEST(test_convert_XC_to_arabic)
{
    int number;
    number = convert_from_roman("XC");
    ck_assert_int_eq(number, 90);
}
END_TEST

START_TEST(test_convert_C_to_arabic)
{
    int number;
    number = convert_from_roman("C");
    ck_assert_int_eq(number, 100);
}
END_TEST

START_TEST(test_convert_CD_to_arabic)
{
    int number;
    number = convert_from_roman("CD");
    ck_assert_int_eq(number, 400);
}
END_TEST

START_TEST(test_convert_D_to_arabic)
{
    int number;
    number = convert_from_roman("D");
    ck_assert_int_eq(number, 500);
}
END_TEST

START_TEST(test_convert_CM_to_arabic)
{
    int number;
    number = convert_from_roman("CM");
    ck_assert_int_eq(number, 900);
}
END_TEST

START_TEST(test_convert_M_to_arabic)
{
    int number;
    number = convert_from_roman("M");
    ck_assert_int_eq(number, 1000);
}
END_TEST

Suite * make_conversion_to_suite(void) {
    Suite *s;
    TCase *tc_convert_to;

    s = suite_create("convert_to");

    /* Core test case */
    tc_convert_to = tcase_create("to");

    tcase_add_test(tc_convert_to, test_convert_I_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_II_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_V_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_X_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_L_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_C_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_D_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_M_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_IV_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_IX_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_XL_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_XC_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_CD_to_arabic);
    tcase_add_test(tc_convert_to, test_convert_CM_to_arabic);

    suite_add_tcase(s, tc_convert_to);

    return s;
}
