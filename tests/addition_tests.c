#include <check.h>
#include "../src/roman.h"

START_TEST(sum_of_I_and_I_is_II)
{
    char roman_numeral[16];

    add_roman("I", "I", roman_numeral);
    ck_assert_str_eq(roman_numeral, "II");
}
END_TEST

START_TEST(sum_of_I_and_II_is_III)
{
    char roman_numeral[16];

    add_roman("I", "II", roman_numeral);
    ck_assert_str_eq(roman_numeral, "III");
}
END_TEST

START_TEST(sum_of_I_and_V_is_VI)
{
    char roman_numeral[16];

    add_roman("I", "V", roman_numeral);
    ck_assert_str_eq(roman_numeral, "VI");
}
END_TEST

START_TEST(sum_of_V_and_I_is_VI)
{
    char roman_numeral[16];

    add_roman("V", "I", roman_numeral);
    ck_assert_str_eq(roman_numeral, "VI");
}
END_TEST

START_TEST(sum_of_IV_and_II_is_VI)
{
    char roman_numeral[16];

    add_roman("IV", "II", roman_numeral);
    ck_assert_str_eq(roman_numeral, "VI");
}
END_TEST

START_TEST(sum_of_IV_and_I_is_V)
{
    char roman_numeral[16];

    add_roman("IV", "I", roman_numeral);
    ck_assert_str_eq(roman_numeral, "V");
}
END_TEST

START_TEST(sum_of_IV_and_V_is_IX)
{
    char roman_numeral[16];

    add_roman("IV", "V", roman_numeral);
    ck_assert_str_eq(roman_numeral, "IX");
}
END_TEST

START_TEST(sum_of_CCCLXIX_and_DCCCXLV_is_MCCXIV)
{
    char roman_numeral[32];

    add_roman("CCCLXIX", "DCCCXLV", roman_numeral);
    ck_assert_str_eq(roman_numeral, "MCCXIV");
}
END_TEST

Suite * make_addition_suite(void) {
    Suite *s;
    TCase *tc_sum;

    s = suite_create("roman_add");

    /* Core test case */
    tc_sum = tcase_create("sum");

    tcase_add_test(tc_sum, sum_of_I_and_I_is_II);
    tcase_add_test(tc_sum, sum_of_I_and_II_is_III);
    tcase_add_test(tc_sum, sum_of_I_and_V_is_VI);
    tcase_add_test(tc_sum, sum_of_V_and_I_is_VI);
    tcase_add_test(tc_sum, sum_of_IV_and_II_is_VI);
    tcase_add_test(tc_sum, sum_of_IV_and_I_is_V);
    tcase_add_test(tc_sum, sum_of_IV_and_V_is_IX);
    tcase_add_test(tc_sum, sum_of_CCCLXIX_and_DCCCXLV_is_MCCXIV);

    suite_add_tcase(s, tc_sum);

    return s;
}
