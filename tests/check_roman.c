#include <stdlib.h>
#include <check.h>

#include "addition_tests.c"
#include "subtraction_tests.c"
#include "conversion_from_tests.c"
#include "conversion_to_tests.c"
#include "validation_tests.c"

int main(void) {
    int number_failed;
    SRunner *sr;

    sr = srunner_create(make_addition_suite());
    srunner_add_suite(sr, make_subtraction_suite());
    srunner_add_suite(sr, make_validation_suite());
    srunner_add_suite(sr, make_conversion_to_suite());
    srunner_add_suite(sr, make_conversion_from_suite());

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
