#ifndef VALIDATOR_H
#define VALIDATOR_H

static const char roman_pattern[] = "^M{0,3}(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[VX]|V?I{0,3})$";

int validate_roman(char*);

#endif
