#include <stdlib.h>
#include <string.h>

#include "conversion.h"

#define append_digit(loop, number, lower_bound, numeral, digit) \
    loop (number >= lower_bound) { \
      strcat(numeral, digit); \
      number -= lower_bound; \
    }

void convert_to_roman(unsigned short int arabic_number, char *roman_numeral) {
    *roman_numeral = 0;

    append_digit(while, arabic_number, 1000, roman_numeral, "M")

    append_digit(if, arabic_number, 900, roman_numeral, "CM")
    append_digit(if, arabic_number, 500, roman_numeral, "D")
    append_digit(if, arabic_number, 400, roman_numeral, "CD")

    append_digit(while, arabic_number, 100, roman_numeral, "C")

    append_digit(if, arabic_number, 90, roman_numeral, "XC")
    append_digit(if, arabic_number, 50, roman_numeral, "L")
    append_digit(if, arabic_number, 40, roman_numeral, "XL")

    append_digit(while, arabic_number, 10, roman_numeral, "X")

    append_digit(if, arabic_number, 9, roman_numeral, "IX")
    append_digit(if, arabic_number, 5, roman_numeral, "V")
    append_digit(if, arabic_number, 4, roman_numeral, "IV")

    append_digit(while, arabic_number, 1, roman_numeral, "I")

}
#undef append_digit


#define append_arabic(numeral, digit, numeral_value, total, numeral_length) \
    if (strncmp(numeral+i, digit, numeral_length) == 0) { \
        total += numeral_value; \
        i += numeral_length - 1; \
    }

int convert_from_roman(char *roman_numeral) {
    int arabic_total = 0;
    int length;
    length = strlen(roman_numeral);

    for (int i=0; i<length; i++) {
        append_arabic(roman_numeral, "M", 1000, arabic_total, 1)
        else append_arabic(roman_numeral, "CM", 900, arabic_total, 2)
        else append_arabic(roman_numeral, "D", 500, arabic_total, 1)
        else append_arabic(roman_numeral, "CD", 400, arabic_total, 2)
        else append_arabic(roman_numeral, "C", 100, arabic_total, 1)
        else append_arabic(roman_numeral, "XC", 90, arabic_total, 2)
        else append_arabic(roman_numeral, "L", 50, arabic_total, 1)
        else append_arabic(roman_numeral, "XL", 40, arabic_total, 2)
        else append_arabic(roman_numeral, "X", 10, arabic_total, 1)
        else append_arabic(roman_numeral, "IX", 9, arabic_total, 2)
        else append_arabic(roman_numeral, "V", 5, arabic_total, 1)
        else append_arabic(roman_numeral, "IV", 4, arabic_total, 2)
        else {
            arabic_total += 1;
        }

    }
    return arabic_total;
}

#undef append_arabic
