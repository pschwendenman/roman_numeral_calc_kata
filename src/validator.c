#include <regex.h>

#include "validator.h"

int validate_roman(char* numeral) {
    int result;
    regex_t regex;

    regcomp(&regex, roman_pattern, REG_EXTENDED);

    result = regexec(&regex, numeral, 0, 0, 0);

    regfree(&regex);

    return (result != REG_NOMATCH);
}
