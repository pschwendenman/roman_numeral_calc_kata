#include "conversion.h"
#include "roman.h"
#include "validator.h"

void add_roman(char* first, char* second, char* answer) {
  answer[0] = '\0';
  if (validate_roman(first) && validate_roman(second)) {
    int result = convert_from_roman(first) + convert_from_roman(second);
    if (result > 0 && result < LARGEST_NUMERAL) {
        convert_to_roman(result, answer);
    }
  }
}

void subtract_roman(char* first, char* second, char* answer) {
  answer[0] = '\0';
  if (validate_roman(first) && validate_roman(second)) {
    int result = convert_from_roman(first) - convert_from_roman(second);
    if (result > 0 && result < LARGEST_NUMERAL) {
        convert_to_roman(result, answer);
    }
  }
}
