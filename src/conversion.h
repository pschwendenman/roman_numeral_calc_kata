#ifndef CONVERSION_H
#define CONVERSION_H

void convert_to_roman(unsigned short int, char*);
int convert_from_roman(char*);

#endif
