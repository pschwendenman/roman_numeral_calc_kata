#ifndef ROMAN_H
#define ROMAN_H

static const int LARGEST_NUMERAL = 3999;

void add_roman(char*, char*, char*);
void subtract_roman(char*, char*, char*);

#endif
